'use strict';
$(function(){
    $('#btn_subir').on('click',function(){
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
    });
    $(window).on('scroll',function(){
        if( $(this).scrollTop() > 0 ) {
            $('#btn_subir').fadeIn(250);
        } else {
            $('#btn_subir').fadeOut(250);
        }
    });
    $('.navbar-nav li').on('click',function(){
        $('.navbar-nav li').removeClass('active');
        $(this).addClass('active');
    });
});