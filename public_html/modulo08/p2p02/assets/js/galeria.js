'use sctrict';
if (typeof jQuery === "undefined" ) {
    throw new Error("Galeria requiere jQuery");
}

/**
 * Clase para implementar la galería de imágenes
 * @param {object} config con los parámetros de configuración de la galería.
 */
function Galeria(config) {
    if (!config) {
        alert('No se puede iniciar la galeria porque no se ha indicado la configuración inicial de la galería');
        throw new Error("No se ha indicado la configuración inicial de la galería");
    }
    this.enlaces = config.enlaces;
    this.info_persona = $(config.info_persona);
    this.frase = $(config.frase);
    this.foto = $(config.foto);
    this.datos;
    this.cambiar = true;
    this.actual = 0;
    this.timer = undefined;
    this.tiempo = config.tiempo * 1000;
    this.form_datos = {
        frase:$(config.form_frase),
        foto:$(config.form_foto),
        persona:$(config.form_persona),
        div:$(config.form_div),
    };
    this.botonera = $(config.botonera);
    this.init_datos();
}

/**
 * Inicializa los datos de la galería
 */
Galeria.prototype.init_datos = function() {
    this.datos = localStorage.getItem('datos_galeria');
    if (!this.datos) {
        this.reset_datos();
    } else {
        this.datos = JSON.parse(this.datos);
    }
}
/**
 * Inicializa los datos a uná configuración por defecto
 * @return {boolean} indicando si se han guardado los datos.
 */
Galeria.prototype.reset_datos = function() {
    this.datos = [
       { persona:"Buddha - बुद्धा",
         frase:"En la confrontación entre el arrollo y la roca, el arrollo siempre ganará, no por la fuerza, sino por la persistencia.",
         foto:"http://www.imagexia.com/img/Cara-de-Buda.jpg"
       },
       { persona:"Khalil Gibran - جبران خليل جبران بن ميخائل بن سعد",
         frase:"El silencio del envidioso está lleno de ruidos.",
         foto:"http://annurtv.com/sitio/wp-content/uploads/2016/04/kahlil-gibran.jpg"
       },
       { persona:"Confucio - 孔子",
         frase: "Todo tiene belleza pero no todo el mundo la puede ver.",
         foto:"http://3.bp.blogspot.com/-VlsuMSoivLU/VeMlD-LymUI/AAAAAAABKPU/Q8PYwsFbqxg/s1600/confucio.jpeg"
       },
       { persona:"Lev Nikoláievich Tolstói - Лев Николаевич Толстой",
         frase:"Mi felicidad consiste en que sé apreciar lo que tengo y no deseo con exceso lo que no tengo.",
         foto:"http://malba.s3-website-sa-east-1.amazonaws.com/wp-content/uploads/2014/09/tolstoi-05.jpg"
       },
       { persona:"Platón - Πλάτων",
         frase:"El más importante y principal negocio público es la buena educación de la juventud.",
         foto:"https://s-media-cache-ak0.pinimg.com/236x/ee/c4/f3/eec4f3420f7024c58f1b44de233d8ecd.jpg"
       },
       { persona:"Henrik Ibsen - hɛnɾɪk ˈjoːhɑn ˈɪpsən",
         frase:"Si dudas de ti mismo, estás vencido de antemano.",
         foto:"https://ebooks.adelaide.edu.au/i/ibsen/henrik/gosse/images/bust2.jpg"
       }
    ];
    return this.guardar_datos();
}

/**
 * Guarda los datos de la galería en local
 * @return {boolean} indicando si se han guardado los datos.
 */
Galeria.prototype.guardar_datos = function() {
    localStorage.setItem('datos_galeria',JSON.stringify(this.datos));
    return true;
}

/**
 * Pliega el formulario de edición
 */
Galeria.prototype.plegar_form = function() {
    this.cambiar = true;
    this.form_datos.persona.html('');
    this.form_datos.persona.html('');
    this.form_datos.persona.html('');
    this.form_datos.div.css("display", "none");
}

/**
 * Muestra los botones de enlace de la galería
 */
Galeria.prototype.generar_botonera = function() {
    var self = this;
    this.botonera.html("");
    this.datos.forEach(function(elem,i) {
        self.botonera.append('<li><a class="nav-slider" data-cont_elem="'+i+'"></a></li>');
    });
}

/**
 * Para el slide y abre el formulario en caso de que esté cerrado. Si está abierto, lo cierra y olvida lo que se haya cambiado sin guardar.
 */
Galeria.prototype.editar_cita = function() {
    if (this.cambiar) {
        this.cambiar = false;
        this.form_datos.persona.html(this.datos[this.actual].persona);
        this.form_datos.frase.html(this.datos[this.actual].frase);
        this.form_datos.foto.html(this.datos[this.actual].foto);
        this.form_datos.div.css("display", "block");
    } else {
        this.plegar_form();
    }
}

/**
 * Muestra la imagen indicada
 * @param  {integer} pos posición en el array de datos de la imagen a mostrar.
 *                       Si no se indica valor, se toma la posición actual
 */
Galeria.prototype.select = function(pos){
    pos = typeof pos !== 'undefined' ?  pos : this.actual;
    var desp = 0;
    var self = this;
    var enlaces = $(galeria.enlaces);
    if (this.cambiar) {
        this.actual = pos;
        enlaces.removeClass("on off");
        setTimeout(function() {
            enlaces.addClass(function(j){
                return (j===pos) ? "on":"off";
            });
        });
        this.info_persona.html(this.datos[pos].persona);
        this.frase.html(this.datos[pos].frase);
        this.foto.attr("src", this.datos[pos].foto);
        desp++;
    }
    clearTimeout(this.timer);
    this.timer = setTimeout(function(){
        self.select((pos + desp) % self.datos.length);
    }, this.tiempo);
}

/**
 * Elimina del slider la imagen actual
 * @return {boolean} indicando si se ha eliminado la imagen o no
 */
Galeria.prototype.eliminar_cita = function() {
    var resultado = false;
    if (this.datos.length === 1) {
        alert('No puedes eliminar la última cita');
    } else {
        this.datos.splice(this.actual,1);
        if (this.actual > 0 ) {
            --this.actual;
        }
        resultado = this.guardar_datos();
        this.generar_botonera();
        this.plegar_form();
        this.select();
    }
    return resultado;
}

/**
 * Actualiza los datos de la cita que se le indica
 * @param  {integer} pos posíción en el array de datos de la cita que se quiere modificar
 *                       Si no se indica una posición, toma la posición actual de la galería.
 */
Galeria.prototype.actualizar_cita = function(pos) {
    pos = typeof pos !== 'undefined' ?  pos : this.actual;
    this.datos[pos].persona = this.form_datos.persona.html();
    this.datos[pos].frase = this.form_datos.frase.html();
    this.datos[pos].foto = this.form_datos.foto.html();
    this.guardar_datos();
    this.plegar_form();
    this.select();
}

/**
 * Añade una nueva cita al final de la galería
 */
Galeria.prototype.nueva_cita = function() {
    this.actual = this.datos.push({
        persona:this.form_datos.persona.html(),
        frase:this.form_datos.frase.html(),
        foto:this.form_datos.foto.html()
    }) - 1;
    this.guardar_datos();
    this.plegar_form();
    this.generar_botonera();
    this.select();
};

/**
 * Borra la base de datos local y restaura a los valores iniciales.
 */
Galeria.prototype.restaurar_citas = function() {
    this.reset_datos();
    this.generar_botonera();
    this.select(0);
}